package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance;
	private static Connection connection;
	private static final String FIND_ALL_USERS_REQUEST = "SELECT * FROM users";
	private static final String FIND_ALL_TEAMS_REQUEST = "SELECT * FROM teams";
	private static final String FIND_TEAM_BY_NAME_REQUEST = "SELECT * FROM teams WHERE name = ?";
	private static final String FIND_USER_BY_NAME_REQUEST = "SELECT * FROM users WHERE login = ?";
	private static final String FIND_TEAM_BY_ID_REQUEST = "SELECT * FROM teams WHERE id = ?";
	private static final String FIND_USER_BY_ID_REQUEST = "SELECT * FROM users WHERE id = ?";
	private static final String INSERT_USER_REQUEST = "INSERT INTO users (login) VALUES(?)";
	private static final String INSERT_TEAM_REQUEST = "INSERT INTO teams (name) VALUES(?)";
	private static final String INSERT_TEAMS_TO_USER_REQUEST = "INSERT INTO users_teams (user_id,team_id) VALUES(?,?)";
	private static final String SELECT_LAST_INSERT_ID_REQUEST = "SELECT LAST_INSERT_ID() AS id";
	public static final String SELECT_ALL_USER_TEAMS_REQUEST = "SELECT * FROM teams t WHERE t.id IN (SELECT ut.team_id FROM users_teams ut WHERE user_id = ?)";
	private static final String DELETE_TEAM_REQUEST = "DELETE FROM teams WHERE id = ?";
	private static final String DELETE_USER_REQUEST = "DELETE FROM users WHERE id = ?";
	private static final String UPDATE_TEAM_REQUEST = "UPDATE teams SET name = ? WHERE id = ?";



	public static synchronized DBManager getInstance() {
		if (instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try {
			ConnectionManager connectionManager = new ConnectionManager(new FileInputStream("app.properties"));
			connection = connectionManager.getConnection();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public List<User> findAllUsers() throws DBException {
		try (PreparedStatement statement = connection.prepareStatement(FIND_ALL_USERS_REQUEST)){
			ResultSet resultSet = statement.executeQuery();
			List<User> userList = new ArrayList<>();

			while (resultSet.next()) {
				userList.add(parseUser(resultSet));
			}
			return userList;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	private User parseUser(ResultSet resultSet) throws SQLException {
		return new User(resultSet.getInt("id"), resultSet.getString("login"));
	}

	public boolean insertUser(User user) throws DBException {
		if (user == null){
			return false;
		}
		try (PreparedStatement statement = connection.prepareStatement(INSERT_USER_REQUEST, Statement.RETURN_GENERATED_KEYS)){

			statement.setString(1, user.getLogin());
			statement.execute();
			ResultSet generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				user.setId(generatedKeys.getInt(1));
			}
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try {
			boolean anyDeleted = false;
			for (User user : users) {
				if (deleteUser(user)) {
					anyDeleted = true;
				}
			}
			return anyDeleted;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean deleteUser(User user) throws DBException, SQLException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_REQUEST)) {
			preparedStatement.setInt(1, user.getId());
			if (getUserById(user.getId()) != null) {
				preparedStatement.execute();
				return true;
			}
		}
		return false;
	}


	private User getUserById(int id) throws SQLException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_ID_REQUEST)) {
			preparedStatement.setInt(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return parseUser(resultSet);
			}
		}
		return null;
	}


	public User getUser(String login) throws DBException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER_BY_NAME_REQUEST);){
			preparedStatement.setString(1, login);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return parseUser(resultSet);
			}
			return null;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}


	public Team getTeam(String name) throws DBException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(FIND_TEAM_BY_NAME_REQUEST);){
			preparedStatement.setString(1, name);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return parseTeam(resultSet);
			}
			return null;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public Team getTeamById(int id) throws SQLException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(FIND_TEAM_BY_ID_REQUEST)) {
			preparedStatement.setInt(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				return parseTeam(resultSet);
			}
		}

		return null;
	}


	public List<Team> findAllTeams() throws DBException {
		try (PreparedStatement statement = connection.prepareStatement(FIND_ALL_TEAMS_REQUEST)){
			ResultSet resultSet = statement.executeQuery();
			List<Team> teamList = new ArrayList<>();

			while (resultSet.next()) {
				teamList.add(parseTeam(resultSet));
			}

			return teamList;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	private Team parseTeam(ResultSet resultSet) throws SQLException {
		return new Team(resultSet.getInt("id"), resultSet.getString("name"));
	}

	public boolean insertTeam(Team team) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement(INSERT_TEAM_REQUEST, Statement.RETURN_GENERATED_KEYS)){
			if (team == null) {
				return false;
			}
			statement.setString(1, team.getName());
			statement.execute();
			ResultSet generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				team.setId(generatedKeys.getInt(1));
			}
			return true;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		if (Objects.isNull(user) || Objects.isNull(teams)){
			return false;
		}
		PreparedStatement preparedStatement = null;
		try {
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(INSERT_TEAMS_TO_USER_REQUEST);
			for (Team team : teams) {
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, team.getId());
				preparedStatement.addBatch();
				preparedStatement.clearParameters();
			}

			preparedStatement.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException(e.getMessage(),e.getCause());
		} finally {
			try {
				connection.setAutoCommit(true);
				if (!Objects.isNull(preparedStatement)){
					preparedStatement.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}


		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USER_TEAMS_REQUEST)){

			preparedStatement.setInt(1, user.getId());
			ResultSet set = preparedStatement.executeQuery();
			List<Team> teamList = new ArrayList<>();
			while (set.next()) {
				teamList.add(parseTeam(set));
			}
			return teamList;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean deleteTeam(Team team) throws DBException{
		try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TEAM_REQUEST)){

			preparedStatement.setInt(1, team.getId());
			if (getTeamById(team.getId()) != null) {
				preparedStatement.execute();
				return true;
			}
			return false;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TEAM_REQUEST)){
			if (getTeamById(team.getId()) != null) {
				preparedStatement.setString(1, team.getName());
				preparedStatement.setInt(2, team.getId());
				preparedStatement.execute();

				return true;
			}
			return false;
		}
		catch (SQLException e){
			throw new DBException(e.getMessage(), e.getCause());
		}
	}



}
